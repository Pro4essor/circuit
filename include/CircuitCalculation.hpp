#ifndef CIRCUIT_CALCULATION
#define CIRCUIT_CALCULATION

#define eps (std::numeric_limits<float>::epsilon())

#include <vector>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <limits>

class Circuit
{
public:
    enum class ConnectionMatrix
    {
        Complete,
        Reduced
    };

private:
    using Star = std::pair<size_t, std::vector<std::pair<size_t, std::pair<size_t, size_t>>>>;

   std::vector<std::vector<float>> m_circuit;

    template<class From, class To> 
    To Convert(const From& from)
    {
        std::stringstream buffer;
        buffer << from;
        To ans;
        buffer >> ans;

        return ans;
    }

    std::vector<std::string> Split(const std::string &line, char delim) 
    {
        std::vector<std::string> elements;
        std::istringstream iss(line);
        std::string item;
        while (std::getline(iss, item, delim)) 
            elements.push_back(item);

        return elements;
    }

    std::vector<std::pair<size_t, float>> ParseLine(const std::string& line)
    {
        std::vector<std::pair<size_t, float>> ans;
        for (auto &&elem : Split(line, ' '))
        {
            if (elem.size() == 0)
                continue;

            auto values = Split(elem, ':');         
            ans.push_back({Convert<std::string, float>(values[0]), values.size() == 2 ? Convert<std::string, float>(values[1]) : 0.f});
        }

        return ans;
    }

    void init(const std::string& filePath, ConnectionMatrix type)
    {
        std::ifstream fin(filePath);
        if (!fin.is_open())
            throw std::runtime_error("Eror when opening file");

        size_t size, enter, exit;
        std::string temp;

        if (type == ConnectionMatrix::Complete)
        {
            fin >> size;
            for (size_t i = 0; i < size; ++i)
            {
                m_circuit.push_back({});
                for (size_t j = 0; j < size; ++j)
                {
                    fin >> temp;
                    m_circuit.back().push_back(temp == "-" ? -1 : Convert<std::string, float>(temp));
                }
            }
            return;
        }

        std::getline(fin, temp);
        auto values = Split(temp, ' ');
        size = Convert<std::string, size_t>(values[0]);
        enter = Convert<std::string, size_t>(values[1]);
        exit = Convert<std::string, size_t>(values[2]);

        m_circuit.push_back(std::vector<float>(size + 2, -1.f));
        m_circuit[0][enter] = 0.f;
        for (size_t i = 1; i <= size; ++i)
        {
            m_circuit.push_back(std::vector<float>(size + 2, -1.f));
            std::getline(fin, temp);
            for (auto &&node : ParseLine(temp))
                m_circuit[i][node.first] = node.second;
        }
        m_circuit.push_back(std::vector<float>(size + 2, -1.f));
        m_circuit[size + 1][exit] = 0.f;

        for (size_t row = 0; row < size + 2; ++row)
        {
            for (size_t column = 0; column < size + 2; ++column)
            {
                auto first = m_circuit[row][column];
                auto second = m_circuit[column][row];

                if (first < second)
                    std::swap(first, second);

                m_circuit[row][column] = m_circuit[column][row] = first;
            }
        }
    }

    std::vector<size_t> SeriesElement(size_t point) const // throught without
    {
        bool isThroughResistance = false;
        bool isWithoutResistance = false;

        size_t throught, without;
        for (size_t i = 0; i < m_circuit.size(); ++i)
        {
            if (m_circuit[point][i] <= -eps)
                continue;
            
            if (std::abs(m_circuit[point][i]) < eps)
            {
                without = i;
                isWithoutResistance = !isWithoutResistance;
                if (!isWithoutResistance)
                    return {};

                continue;
            }
            
            throught = i;
            isThroughResistance = !isThroughResistance;
            if (!isThroughResistance)
                return {};
        }
        
        return (isThroughResistance && isWithoutResistance) ? std::vector<size_t>({throught, without}) : std::vector<size_t>{};
    }

    std::vector<size_t> SearchSeries() const // --a|b--c|d---
    {
        for (size_t i = 0; i < m_circuit.size(); ++i)
        {
            auto ac = SeriesElement(i);
            if (!ac.size())
                continue;
            
            auto db = SeriesElement(ac[1]);
            if (!db.size())
                continue;

            return {ac[0], db[1], ac[1], db[0]};
        }

        return {};
    }

    bool RecalculateSeries() // --a|b--c|d---
    {
        auto abcd = SearchSeries();
        if (!abcd.size())
            return false;
        
        m_circuit[abcd[3]][abcd[0]] = m_circuit[abcd[0]][abcd[3]] = m_circuit[abcd[0]][abcd[1]] + m_circuit[abcd[2]][abcd[3]];
        
        Erase(std::min(abcd[1], abcd[2]));
        Erase(std::max(abcd[1], abcd[2]) - 1);

        return true;
    }

    /*
         e f
       +--|--+
       |     |
     --+a   d+--
       |     |
       +--|--+
         b c
    */
    void RecalculateParallel(size_t b, size_t c, size_t e, size_t f)
    {
        m_circuit[b][c] = m_circuit[c][b] = 1.f / (1.f / m_circuit[b][c] + 1.f / m_circuit[e][f]);
        Erase(std::min(e, f));
        Erase(std::max(e, f) - 1);
    }

    bool RecalculateParallel()
    {
        for (size_t i = 0; i < m_circuit.size(); ++i)
        { 
            auto ca = SeriesElement(i);
            if (!ca.size())
                continue;
            
            auto bd = SeriesElement(ca[0]);
            if (!bd.size())
                continue;

            for (auto &&pointFromA : GetConnectedPoints(ca[1]))
            {
                for (auto &&pointFromD : GetConnectedPoints(bd[1]))
                    if (m_circuit[pointFromA][pointFromD] > eps && pointFromA != bd[0] && pointFromD != ca[0])
                    {
                        RecalculateParallel(bd[0], ca[0], pointFromA, pointFromD);
                        return true;
                    }
            } 
        }
        return false;
    }

    std::vector<std::pair<size_t, std::pair<size_t, size_t>>> GetConnectedNodesThroughResistance(size_t node) const //  a +---- b |||| c -----+ d
    {
        std::vector<std::pair<size_t, std::pair<size_t, size_t>>> ans;
        for (auto &&point : GetConnectedPoints(node))    
        {                                                           
            auto ca = SeriesElement(point);
            if (!ca.size())
                continue;
            
            auto bd = SeriesElement(ca[0]);
            if (!bd.size() || !IsNode(bd[1]))
                continue;
            
            ans.push_back({bd[1], {bd[0], ca[0]}});     
        }

        return ans;
    }


    /*
        d 0
         / \
        /   \
    c ---   --- e
    b ---   --- f
      /       \
     /         \
    0----||||---0 
    a   i    h  g
    */
    // std::vector<size_t> SearchTriangle() const
    // {
    //     for (size_t i = 0; i < m_circuit.size(); ++i)
    //     {                                                           
    //         auto ca = SeriesElement(i);
    //         if (!ca.size() || !IsNode(ca[1]))
    //             continue;

    //         auto bd = SeriesElement(ca[0]);
    //         if (!bd.size() || !IsNode(bd[1]))
    //             continue;

    //         auto nodesFromA = GetConnectedNodesThroughResistance(ca[1]);
    //         auto nodesFromD = GetConnectedNodesThroughResistance(bd[1]);

    //         for (auto &&nodeElemA : nodesFromA)
    //             for (auto &&nodeElemD : nodesFromD)
    //                 if (nodeElemA.first == nodeElemD.first)
    //                     return {ca[1], bd[0], ca[0], bd[1], nodeElemD.second.first, nodeElemD.second.second, nodeElemD.first, nodeElemA.second.second, nodeElemA.second.first};
    //     }
    //     return {};
    // }

    std::vector<std::vector<size_t>> SearchTriangles() const
    {
        std::vector<std::vector<size_t>> ans;
        for (size_t i = 0; i < m_circuit.size(); ++i)
        {                                                           
            auto ca = SeriesElement(i);
            if (!ca.size() || !IsNode(ca[1]))
                continue;

            auto bd = SeriesElement(ca[0]);
            if (!bd.size() || !IsNode(bd[1]))
                continue;

            auto nodesFromA = GetConnectedNodesThroughResistance(ca[1]);
            auto nodesFromD = GetConnectedNodesThroughResistance(bd[1]);

            for (auto &&nodeElemA : nodesFromA)
                for (auto &&nodeElemD : nodesFromD)
                    if (nodeElemA.first == nodeElemD.first)
                        ans.push_back({ca[1], bd[0], ca[0], bd[1], nodeElemD.second.first, nodeElemD.second.second, nodeElemD.first, nodeElemA.second.second, nodeElemA.second.first});
        }
        return ans;
    }

    void AppendPoint()
    {
        for (size_t i = 0; i < m_circuit.size(); i++)
            m_circuit[i].push_back(-1.f);
        
        m_circuit.push_back(std::vector<float>(m_circuit.size() + 1, -1.f));
    }

    // bool RecalculateTriangleWithStar()
    // {
    //     auto triangle = SearchTriangle();
    //     if (!triangle.size())
    //         return false;

    //     auto resistanceBC = m_circuit[triangle[1]][triangle[2]];
    //     auto resistanceEF = m_circuit[triangle[4]][triangle[5]];
    //     auto resistanceHI = m_circuit[triangle[7]][triangle[8]];
        
    //     m_circuit[triangle[1]][triangle[2]] = m_circuit[triangle[2]][triangle[1]] = (resistanceBC * resistanceHI) / (resistanceBC + resistanceEF + resistanceHI);
    //     m_circuit[triangle[4]][triangle[5]] = m_circuit[triangle[5]][triangle[4]] = (resistanceBC * resistanceEF) / (resistanceBC + resistanceEF + resistanceHI);
    //     m_circuit[triangle[7]][triangle[8]] = m_circuit[triangle[8]][triangle[7]] = (resistanceHI * resistanceEF) / (resistanceBC + resistanceEF + resistanceHI);

    //     AppendPoint();
    //     m_circuit[triangle[2]][triangle[3]] = m_circuit[triangle[3]][triangle[2]] = -1.f;
    //     m_circuit[triangle[2]][m_circuit.size() - 1] = m_circuit[m_circuit.size() - 1][triangle[2]] = 0.f;

    //     m_circuit[triangle[5]][triangle[6]] = m_circuit[triangle[6]][triangle[5]] = -1.f;
    //     m_circuit[triangle[5]][m_circuit.size() - 1] = m_circuit[m_circuit.size() - 1][triangle[5]] = 0.f;

    //     m_circuit[triangle[0]][triangle[8]] = m_circuit[triangle[8]][triangle[0]] = -1.f;
    //     m_circuit[triangle[8]][m_circuit.size() - 1] = m_circuit[m_circuit.size() - 1][triangle[8]] = 0.f;

    //     return true;
    // }

    void RecalculateTriangleWithStar(std::vector<size_t> triangle)
    {
        auto resistanceBC = m_circuit[triangle[1]][triangle[2]];
        auto resistanceEF = m_circuit[triangle[4]][triangle[5]];
        auto resistanceHI = m_circuit[triangle[7]][triangle[8]];
        
        m_circuit[triangle[1]][triangle[2]] = m_circuit[triangle[2]][triangle[1]] = (resistanceBC * resistanceHI) / (resistanceBC + resistanceEF + resistanceHI);
        m_circuit[triangle[4]][triangle[5]] = m_circuit[triangle[5]][triangle[4]] = (resistanceBC * resistanceEF) / (resistanceBC + resistanceEF + resistanceHI);
        m_circuit[triangle[7]][triangle[8]] = m_circuit[triangle[8]][triangle[7]] = (resistanceHI * resistanceEF) / (resistanceBC + resistanceEF + resistanceHI);

        AppendPoint();
        m_circuit[triangle[2]][triangle[3]] = m_circuit[triangle[3]][triangle[2]] = -1.f;
        m_circuit[triangle[2]][m_circuit.size() - 1] = m_circuit[m_circuit.size() - 1][triangle[2]] = 0.f;

        m_circuit[triangle[5]][triangle[6]] = m_circuit[triangle[6]][triangle[5]] = -1.f;
        m_circuit[triangle[5]][m_circuit.size() - 1] = m_circuit[m_circuit.size() - 1][triangle[5]] = 0.f;

        m_circuit[triangle[0]][triangle[8]] = m_circuit[triangle[8]][triangle[0]] = -1.f;
        m_circuit[triangle[8]][m_circuit.size() - 1] = m_circuit[m_circuit.size() - 1][triangle[8]] = 0.f;
    }

    /*
        d 0
         /|\
        / | \
    c --- | --- e
    b --- O --- f
      / /   \ \
     / /     \ \
    0----||||---0 
    a   i    h  g
    */
    // bool RecalculateStarWithTriangle()
    // {
    //     for (size_t i = 0; i < m_circuit.size(); ++i)
    //     {
    //         if (!IsNode(i) || IsExtremePoint(i))
    //             continue;
            
    //         auto points = GetConnectedNodesThroughResistance(i);
    //         if (points.size() != 3 || GetConnectedPoints(i).size() != 3)
    //             continue;

    //         auto resistanceOA = m_circuit[points[0].second.first][points[0].second.second];
    //         auto resistanceOD = m_circuit[points[1].second.first][points[1].second.second];
    //         auto resistanceOG = m_circuit[points[2].second.first][points[2].second.second];

    //         m_circuit[points[0].second.first][points[0].second.second] = 
    //         m_circuit[points[0].second.second][points[0].second.first] = resistanceOA + resistanceOD + (resistanceOA * resistanceOD) / resistanceOG;

    //         m_circuit[points[1].second.first][points[1].second.second] = 
    //         m_circuit[points[1].second.second][points[1].second.first] = resistanceOD + resistanceOG + (resistanceOD * resistanceOG) / resistanceOA;

    //         m_circuit[points[2].second.first][points[2].second.second] = 
    //         m_circuit[points[2].second.second][points[2].second.first] = resistanceOA + resistanceOG + (resistanceOA * resistanceOG) / resistanceOD;

    //         m_circuit[points[0].second.first][points[1].first] = m_circuit[points[1].first][points[0].second.first] = 0.f;
    //         m_circuit[points[1].second.first][points[2].first] = m_circuit[points[2].first][points[1].second.first] = 0.f;
    //         m_circuit[points[2].second.first][points[0].first] = m_circuit[points[0].first][points[2].second.first] = 0.f;

    //         Erase(i);
    //         return true;
    //     }

    //     return false;
    // }
    void RecalculateStarWithTriangle(Star star)
    {
        size_t vertex = star.first;
        auto points = star.second;

        auto resistanceOA = m_circuit[points[0].second.first][points[0].second.second];
        auto resistanceOD = m_circuit[points[1].second.first][points[1].second.second];
        auto resistanceOG = m_circuit[points[2].second.first][points[2].second.second];

        m_circuit[points[0].second.first][points[0].second.second] = 
        m_circuit[points[0].second.second][points[0].second.first] = resistanceOA + resistanceOD + (resistanceOA * resistanceOD) / resistanceOG;

        m_circuit[points[1].second.first][points[1].second.second] = 
        m_circuit[points[1].second.second][points[1].second.first] = resistanceOD + resistanceOG + (resistanceOD * resistanceOG) / resistanceOA;

        m_circuit[points[2].second.first][points[2].second.second] = 
        m_circuit[points[2].second.second][points[2].second.first] = resistanceOA + resistanceOG + (resistanceOA * resistanceOG) / resistanceOD;

        m_circuit[points[0].second.first][points[1].first] = m_circuit[points[1].first][points[0].second.first] = 0.f;
        m_circuit[points[1].second.first][points[2].first] = m_circuit[points[2].first][points[1].second.first] = 0.f;
        m_circuit[points[2].second.first][points[0].first] = m_circuit[points[0].first][points[2].second.first] = 0.f;

        Erase(vertex);
    }

    bool IsNode(size_t point) const
    {
        auto points = GetConnectedPoints(point);
        if (points.size() == 1)
            return false;

        for (auto &&elem : points)
            if (m_circuit[elem][point] > eps)
                return false;

        return true;
    }

    std::vector<size_t> GetConnectedPoints(size_t point) const
    {
        std::vector<size_t> points;
        for (size_t i = 0; i < m_circuit.size(); ++i)
        {
            if (m_circuit[point][i] > -eps)
                points.push_back(i);
        }

        return points;
    }

    void Collapse(size_t firstPoint, size_t secondPoint)
    {
        if (firstPoint > secondPoint)
            std::swap(firstPoint, secondPoint);

        for (size_t i = 0; i < m_circuit.size(); ++i)
        {
            if (i == firstPoint)
                continue;
            
            m_circuit[firstPoint][i] = std::max(m_circuit[firstPoint][i], m_circuit[secondPoint][i]);
        }
        Erase(secondPoint);
    }

    bool IsExtremePoint(size_t point) const
    {
        for (size_t i = 0; i < m_circuit.size(); ++i)
        {
            auto points = GetConnectedPoints(i);
            if (points.size() == 1 && points[0] == point)
                return true;
        }

        return false;
    }

    bool IsMiddlePoint(size_t point)
    {
        std::vector<size_t> connectedPoints;
        for (size_t i = 0; i < m_circuit.size(); ++i)
        {
            if (m_circuit[point][i] > eps)
                return false;
            
            else if (std::abs(m_circuit[point][i]) < eps)
                connectedPoints.push_back(i);
        }

        if (connectedPoints.size() != 2)
            return false;

        m_circuit[connectedPoints[0]][connectedPoints[1]] = m_circuit[connectedPoints[1]][connectedPoints[0]] = 0.f;
        Erase(point);

        return true;
    }

    void RemoveMiddlePoints() // --|--0--|--
    {
        for (size_t i = 0; i < m_circuit.size(); ++i)
        {
            if (IsMiddlePoint(i))  
                return RemoveMiddlePoints();
        }
    }
    /*
    ----|----
    \  a  b /
     \     /
      \---/
        c
    */
    void RemoveInsignificantResistance()
    {
        
        for (size_t i = 0; i < m_circuit.size(); ++i)
        {
            auto bc = SeriesElement(i);
            if (!bc.size())
                continue;

            auto ac = SeriesElement(bc[0]);
            if (!ac.size())
                continue;
        
            if (bc[1] == ac[1])
            {
                Erase(std::min(ac[0], bc[0]));
                Erase(std::max(ac[0], bc[0]) - 1);
                return RemoveInsignificantResistance();
            }
        }
    }

    void Erase(size_t point)
    {
        auto beginRow = m_circuit.begin();
        std::advance(beginRow, point);
        m_circuit.erase(beginRow);
        for (auto &&row : m_circuit)
        {
            auto beginColumn = row.begin();
            std::advance(beginColumn, point);

            row.erase(beginColumn);
        }
    }

    std::vector<Star> SearchStars() const
    {
        std::vector<Star> ans;
        for (size_t i = 0; i < m_circuit.size(); ++i)
        {
            if (!IsNode(i) || IsExtremePoint(i))
                continue;
            
            auto points = GetConnectedNodesThroughResistance(i);
            if (points.size() != 3 || GetConnectedPoints(i).size() != 3)
                continue;

            ans.push_back({i, points});
        }

        return ans;
    }

    Star SearchStar() const
    {
        for (size_t i = 0; i < m_circuit.size(); ++i)
        {
            if (!IsNode(i) || IsExtremePoint(i))
                continue;
            
            auto points = GetConnectedNodesThroughResistance(i);
            if (points.size() != 3 || GetConnectedPoints(i).size() != 3)
                continue;

            return {i, points};
        }
        return {};
    }

    void RecalculateSeriesAndParallel()
    {
        RemoveInsignificantResistance();
        if (RecalculateSeries())
        {
            while (RecalculateSeries());
            return RecalculateSeriesAndParallel();
        }
        
        if (RecalculateParallel())
        {
            RemoveMiddlePoints();
            while (RecalculateParallel())
                RemoveMiddlePoints();

            return RecalculateSeriesAndParallel();
        }

        return;
    }

    float RecursiveCalculate()
    {
        std::cout << m_circuit.size() << '\n';
        
        if (m_circuit.size() == 4)
            return m_circuit[1][2];
        
        auto circuit = m_circuit;
        auto stars = SearchStars();
        auto triangles = SearchTriangles();

        if (!stars.size() && !triangles.size())
            throw std::runtime_error("Invalid to calculate");

        for (auto &&star : stars)
        {
            auto size = m_circuit.size();
            RecalculateStarWithTriangle(star);
            RecalculateSeriesAndParallel();
            if (size - 1 > m_circuit.size())
                return RecursiveCalculate();
            
            m_circuit = circuit;
        }
        
        for (auto &&triangle : triangles)
        {
            auto size = m_circuit.size();
            RecalculateTriangleWithStar(triangle);
            RecalculateSeriesAndParallel();
            if (size > m_circuit.size())
                return RecursiveCalculate();
            
            m_circuit = circuit;
        }
        
        if (stars.size() == 0)
        {
            RecalculateTriangleWithStar(triangles[rand() % triangles.size()]);
            RecalculateSeriesAndParallel();

            return RecursiveCalculate();
        }
        if (triangles.size() == 0)
        {
            RecalculateStarWithTriangle(stars[rand() % stars.size()]);
            RecalculateSeriesAndParallel();

            return RecursiveCalculate();
        }

        if (rand() % 2 == 0)
        {
            RecalculateTriangleWithStar(triangles[rand() % triangles.size()]);
            RecalculateSeriesAndParallel();
            // auto tr = SearchTriangles();
            // if (tr.size())
            //     RecalculateTriangleWithStar(tr[rand() % tr.size()]);
            // tr = SearchTriangles();
            // if (tr.size())
            //     RecalculateTriangleWithStar(tr[rand() % tr.size()]);
        }
            
        else
        {
            RecalculateStarWithTriangle(stars[rand() % stars.size()]);
            RecalculateSeriesAndParallel();
            // auto st = SearchStars();
            // if (st.size())
            //     RecalculateStarWithTriangle(st[rand() % st.size()]);
            // st = SearchStars();
            // if (st.size())
            //     RecalculateStarWithTriangle(st[rand() % st.size()]);
        }
        
        //RecalculateSeriesAndParallel();

        return RecursiveCalculate();
    }

public:
    Circuit() = default;
    Circuit(const std::string& filePath, ConnectionMatrix type)
        : m_circuit()
    { init(filePath, type); }

    Circuit(const std::vector<std::vector<float>>& circuit)
        : m_circuit(circuit)
    {}
    
    ~Circuit() = default;

    float Calculate()
    {
        // collapse points of equal potential
        auto isCollapse = false;
        do
        {
            for (size_t i = 0; i < m_circuit.size(); ++i)
            {
                if (!IsNode(i))
                    continue;
                
                for (auto &&point : GetConnectedPoints(i))
                {
                    if (IsNode(point))
                    {
                        if (IsExtremePoint(i) && IsExtremePoint(point))
                            return 0.f;
                        
                        Collapse(i, point);
                        isCollapse = true;
                        break;
                    }
                }
                if (isCollapse)
                    break;
            }
            
            if (!isCollapse)
                break;
        } while (!isCollapse);
        
        do
        {
            auto star = SearchStar();
            if (star.second.size() != 0)
            {
                RecalculateStarWithTriangle(star);
                RecalculateSeriesAndParallel();
            }
            else 
                break;
        } while (true);
        
        RecalculateSeriesAndParallel();

        return RecursiveCalculate();
    }

    void Print() const
    {
        for (auto &&row : m_circuit)
        {
            for (auto &&column : row)
            {
                if (column < 0)
                    std::cout <<  std::setw(3) << '-';
                else
                    std::cout <<  std::setw(3) << std::setprecision(2) << column;

                std::cout << " ";
            }
            std::cout << std::endl;
        }
    }
};


#endif