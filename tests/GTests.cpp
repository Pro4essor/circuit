#ifndef GTEST
#define GTEST

#include "gtest/gtest.h"

#include "./../include/CircuitCalculation.hpp"
#include <stdlib.h>

#endif

/*
---||||-----||||----
*/
TEST(Test1, OnlySeries) 
{
	Circuit circuit("series1.txt", Circuit::ConnectionMatrix::Complete);
    EXPECT_FLOAT_EQ(2.f, circuit.Calculate());
}

/*
---||||----||||----||||-----
*/
TEST(Test2, OnlySeries) 
{
	Circuit circuit("series2.txt", Circuit::ConnectionMatrix::Complete);
    EXPECT_FLOAT_EQ(3.f, circuit.Calculate());
}

/*
---||||-----||||----
*/
TEST(Test3, OnlySeries) 
{
	Circuit circuit("series3.txt", Circuit::ConnectionMatrix::Reduced);
    EXPECT_FLOAT_EQ(2.f, circuit.Calculate());
}

/*
    +----||||----+
    |            |
----+            +-------
    |            |
    +----||||----+
*/
TEST(Test1, OnlyParallel) 
{
	Circuit circuit("parallel1.txt", Circuit::ConnectionMatrix::Complete);
    EXPECT_FLOAT_EQ(0.5f, circuit.Calculate());
}

/*
  +-------------------+
  |                   |
--+--||||--+---||||---+---||||--+-----
           |                    |
           +--------------------+
*/
TEST(Test1, SeriesAndParallel) 
{
	Circuit circuit("SeriesAndParallel1.txt", Circuit::ConnectionMatrix::Reduced);
    EXPECT_FLOAT_EQ(1.f / 3, circuit.Calculate());
}

/*
                      |
                      |
      +-----||||------+-------||||-----+
      |               |                |
      |               |               ---
------+              ---              ---
      |              ---               |
      |               |                |
      +----||||-------+--------||||----+        
*/
TEST(Test2, SeriesAndParallel) 
{
	Circuit circuit("SeriesAndParallel2.txt", Circuit::ConnectionMatrix::Reduced);
    EXPECT_FLOAT_EQ(7.f / 11, circuit.Calculate());
}

/*
      +----||||---+
      |           |
------+-----------+------
*/
TEST(Test1, ZeroResistance)
{
    Circuit circuit("ZeroResistance.txt", Circuit::ConnectionMatrix::Reduced);
    EXPECT_FLOAT_EQ(0.f, circuit.Calculate());
}


/*
      +-----||||------+-------||||-----+
      |               |                |
      |               |                |
------+              ---               +-------
      |              ---               |
      |               |                |
      +----||||-------+--------||||----+        
*/
TEST(Test1, Complex)
{
    Circuit circuit("Complex1.txt", Circuit::ConnectionMatrix::Reduced);
    EXPECT_FLOAT_EQ(1.f, circuit.Calculate());
}

TEST(Test2, Complex)
{
    Circuit circuit("Cube.txt", Circuit::ConnectionMatrix::Reduced);
    EXPECT_FLOAT_EQ(7.f / 12, circuit.Calculate());
}

TEST(Test3, Complex)
{
    Circuit circuit("Cube1.txt", Circuit::ConnectionMatrix::Reduced);
    EXPECT_FLOAT_EQ(5.f / 6, circuit.Calculate());
}

TEST(Test4, Complex)
{
    Circuit circuit("Cube2.txt", Circuit::ConnectionMatrix::Reduced);
    EXPECT_FLOAT_EQ(0.75f, circuit.Calculate());
}

TEST(Test5, Complex)
{
    Circuit circuit("Dodecahedron.txt", Circuit::ConnectionMatrix::Reduced);
    EXPECT_FLOAT_EQ(7.f / 6, circuit.Calculate());
}

// TEST(Test6, Complex)
// {
//     Circuit circuit("Ball.txt", Circuit::ConnectionMatrix::Reduced);
//     EXPECT_FLOAT_EQ(0.f, circuit.Calculate());
// }